{ pkgs ? import <nixpkgs> {}
, pythonPackages ? pkgs.python38Packages }:

let
  # f would be what would go in nixpkgs. Which explains the strange style.
  f =
    { buildPythonApplication
    , pytest
    , discord
    }:

    buildPythonApplication rec {
      version = "master";
      pname = "bonkbot";
      src = ./.;

      doCheck = false;
      checkInputs = [ pytest ];
      propagatedBuildInputs = [
        discord
      ];
    };
  drv = pythonPackages.callPackage f {};
in
  drv
