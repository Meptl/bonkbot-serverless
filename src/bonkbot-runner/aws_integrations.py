import boto3

DYNAMODB_TABLE_NAME = 'bonkbot'

def get_jail_for_guild(guild_id):
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMODB_TABLE_NAME)
    data = table.get_item(Key={'guild_id': int(guild_id)})
    try:
        return data['Item']['jail_id']
    except KeyError:
        return None


def set_jail_for_guild(guild_id, jail_id):
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMODB_TABLE_NAME)
    table.put_item(Item={
        'guild_id': int(guild_id),
        'jail_id': int(jail_id)
    })
