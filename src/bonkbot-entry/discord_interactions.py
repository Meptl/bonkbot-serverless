from nacl.signing import VerifyKey
from nacl.exceptions import BadSignatureError

PUBLIC_KEY = 'e4519100a43979b42b95d7bccf0d7dfae222cab82f746c3ceb6fc4b2c35c36d0'
RESPONSE_TYPES =  {
    'PONG': 1,
    'CHANNEL_MESSAGE_WITH_SOURCE': 4,
    'DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE': 5,
    # Below two responses are for component-based interactions.
    'DEFERRED_UPDATE_MESSAGE': 6,
    'UPDATE_MESSAGE': 7,
}


def _verify_signature(event):
    raw_body = event.get("rawBody")
    auth_sig = event['params']['header'].get('x-signature-ed25519')
    auth_ts  = event['params']['header'].get('x-signature-timestamp')

    message = auth_ts.encode() + raw_body.encode()
    verify_key = VerifyKey(bytes.fromhex(PUBLIC_KEY))
    verify_key.verify(message, bytes.fromhex(auth_sig)) # raises an error if unequal


# If this function returns a value, the lambda function should forward it. This
# occurs for ping-pong requests.
def preprocess(event):
    try:
        _verify_signature(event)
    except Exception as e:
        raise Exception(f"[UNAUTHORIZED] Invalid request signature: {e}")

    # Check if message is a ping
    if event.get('body-json').get('type') == 1:
        return { 'type': RESPONSE_TYPES['PONG'] }
    return None


# ephemeal: only the calling user sees the message.
def return_message(message, ephemeral=False):
    response = {
        'type': RESPONSE_TYPES['CHANNEL_MESSAGE_WITH_SOURCE'],
        'data': {
            'tts': False,
            'content': message,
        }
    }
    if ephemeral:
        response['flags'] = 64

    return response

def deferred_message():
    return { 'type': RESPONSE_TYPES['DEFERRED_CHANNEL_MESSAGE_WITH_SOURCE'] }
